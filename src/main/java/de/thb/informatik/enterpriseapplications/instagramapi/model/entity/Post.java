package de.thb.informatik.enterpriseapplications.instagramapi.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long postId;
    @ManyToOne(fetch = FetchType.EAGER)
    private InstagramUser owner;
    private String shortCode;
    private String caption;
    private LocalDateTime timestampCreated;
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "taggedIn", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<InstagramUser> userTags;
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "likedPosts", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<InstagramUser> likers;
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "postsWithHashtag", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Hashtag> hashtags;

    @Override
    @SuppressWarnings("all")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return postId == post.postId &&
                owner.equals(post.owner) &&
                Objects.equals(shortCode, post.shortCode) &&
                timestampCreated.equals(post.timestampCreated);
    }

    @Override
    public int hashCode() {
        return Objects.hash(postId, owner, shortCode, timestampCreated);
    }
}
