package de.thb.informatik.enterpriseapplications.instagramapi.dataaccess;

import de.thb.informatik.enterpriseapplications.instagramapi.dataaccess.exception.EntityNotFoundException;
import de.thb.informatik.enterpriseapplications.instagramapi.model.dto.InstagramUserDto;
import de.thb.informatik.enterpriseapplications.instagramapi.model.entity.InstagramUser;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class InstagramUserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(InstagramUserService.class);
    private final InstagramUserRepository repository;
    private final ModelMapper modelMapper;

    public InstagramUserService(@Autowired InstagramUserRepository repository, @Autowired ModelMapper modelMapper){
        this.modelMapper = modelMapper;
        this.repository = repository;
    }

    public void persistUserDto(InstagramUserDto userDto) {
        InstagramUser user = repository.findByName(userDto.getName()).orElse(new InstagramUser());
        modelMapper.map(userDto, user);
        user.setTimestampUpdated(LocalDateTime.now());
        repository.save(user);
    }

    public void addUser(InstagramUser user){
        try {
            user.setTimestampUpdated(LocalDateTime.now());
            this.repository.save(user);
        } catch (Exception e) {
            LOGGER.error("{} not saved.", user, e);
        }
    }

    public void updateUser(InstagramUserDto userDto) throws EntityNotFoundException {
        InstagramUser user = this.repository.findByName(userDto.getName()).orElseThrow(
                () -> new EntityNotFoundException(String.format("User with name %s not found.", userDto.getName())));
        modelMapper.map(userDto, user);
        repository.save(user);
    }

    public InstagramUser getUser(long id) {
        return this.repository.findById(id).orElse(null);
    }

    public boolean addFollowerToUser(long userId, long followerId) {
        try {
            InstagramUser user = repository.findById(userId).orElseThrow(
                    () -> new EntityNotFoundException("User not found"));
            InstagramUser follower = repository.findById(followerId).orElseThrow(
                    () -> new EntityNotFoundException("User not found"));
            user.addFollower(follower);
            repository.save(user);
            return true;
        } catch (EntityNotFoundException e) {
            LOGGER.error("User not found in INSTAGRAM_USER DB. Follower not saved.", e);
            return false;
        }
    }

    public InstagramUser findUserByName(String name) throws EntityNotFoundException {
        return repository.findByName(name).orElseThrow(
                () -> new EntityNotFoundException(String.format("User with name %s", name)));
    }

    public List<InstagramUser> getUsersWhoFollowUser(InstagramUser user) {
        return repository.findByFollowersUserId(user.getUserId());
    }

    public boolean userExists(long id) {
        return repository.findById(id).isPresent();
    }
    public boolean userExists(String name) {
        return repository.findByName(name).isPresent();
    }
}
