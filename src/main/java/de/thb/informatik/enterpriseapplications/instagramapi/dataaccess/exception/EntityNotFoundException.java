package de.thb.informatik.enterpriseapplications.instagramapi.dataaccess.exception;

import javassist.NotFoundException;

public class EntityNotFoundException extends NotFoundException {
    public EntityNotFoundException(String message) {
        super(message);
    }
    public EntityNotFoundException(String message, Exception e){
        super(message, e);
    }
}
