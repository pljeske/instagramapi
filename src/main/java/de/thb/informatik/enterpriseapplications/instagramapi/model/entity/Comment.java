package de.thb.informatik.enterpriseapplications.instagramapi.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long commentId;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private InstagramUser creator;
    private String text;
    private LocalDateTime timestampCreated;

    @Override
    @SuppressWarnings("all")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return creator.equals(comment.creator) &&
                text.equals(comment.text) &&
                timestampCreated.equals(comment.timestampCreated);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creator, text, timestampCreated);
    }
}
