package de.thb.informatik.enterpriseapplications.instagramapi.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SpringFoxConfig {
//    private static final String CONTROLLER_PACKAGE = "de.thb.informatik.enterpriseapplications.instagramapi.controller";

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(metadata())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant("/api/**"))
                .build();
    }

    private static ApiInfo metadata(){
        return new ApiInfoBuilder()
                .title("Instagram API")
                .description("Your Description")
                .version("0.1")
                .build();
    }
}