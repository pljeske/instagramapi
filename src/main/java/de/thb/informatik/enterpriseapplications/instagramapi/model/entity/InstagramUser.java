package de.thb.informatik.enterpriseapplications.instagramapi.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
//@NamedQuery(name = "InstagramUser.followed_by",
//        query = "select u from INSTAGRAM_ u where u.followers contains ")
public class InstagramUser  {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long userId;
    @NaturalId
    private String name;
    private String businessEmail;
    @Lob
    private Byte[] profilePicture;
    private String biography;
    private LocalDateTime timestampUpdated;
    private boolean isPrivate;
    private boolean isVerified;
    @Builder.Default
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<InstagramUser> followers = new HashSet<>();
    @Builder.Default
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Post> taggedIn = new HashSet<>();
    @Builder.Default
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Post> likedPosts = new HashSet<>();
    @Builder.Default
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "owner", cascade = CascadeType.ALL)
    private List<Post> posts = new ArrayList<>();
    @Builder.Default
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "creator", cascade = CascadeType.ALL)
    private List<Comment> comments = new ArrayList<>();

    public int getNumberOfFollowers(){
        return followers.size();
    }
    public int getNumberOfPosts(){
        return posts.size();
    }

    public void addFollower(InstagramUser follower) {
        if (!this.followers.contains(follower)){
            this.followers.add(follower);
        }
    }

    @Override
    public int hashCode(){
        return Objects.hash(this.name);
    }

    @Override
    @SuppressWarnings("all")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InstagramUser)) return false;
        InstagramUser user = (InstagramUser) o;
        return Objects.equals(getName(), user.getName());
    }
    @Override
    public String toString(){
        return userId + ": " + name;
    }
}
