package de.thb.informatik.enterpriseapplications.instagramapi.dataaccess;

import de.thb.informatik.enterpriseapplications.instagramapi.model.entity.InstagramUser;
import de.thb.informatik.enterpriseapplications.instagramapi.model.entity.InstagramUserProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "users", path = "users",
        excerptProjection = InstagramUserProjection.class)
public interface InstagramUserRepository extends JpaRepository<InstagramUser, Long> {
    @RestResource(rel="find_by_name", path="find_by_name")
    Optional<InstagramUser> findByName(String name);
    @RestResource(rel="followed_by_id", path="followed_by_id")
    List<InstagramUser> findByFollowersUserId(long userId);
    @RestResource(rel="followed_by_name", path="followed_by_name")
    List<InstagramUser> findByFollowersName(String name);
//    Optional<List<InstagramUserProjection>> findAllByFollowersContains(InstagramUser user);
}
