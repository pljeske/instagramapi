package de.thb.informatik.enterpriseapplications.instagramapi.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
public class Carousel extends Post {
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "carousel", cascade = CascadeType.ALL)
    private List<Video> videos;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "carousel", cascade = CascadeType.ALL)
    private List<Image> images;

    @Override
    @SuppressWarnings("all")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Carousel carousel = (Carousel) o;
        return Objects.equals(videos, carousel.videos) &&
                Objects.equals(images, carousel.images);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), videos, images);
    }
}
