package de.thb.informatik.enterpriseapplications.instagramapi.model.entity;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Projection(name = "instagramUserProjection", types = {InstagramUser.class})
public interface InstagramUserProjection {
    long getUserId();
    String getName();
    String getBusinessEmail();
    Byte[] getProfilePicture();
    String getBiography();
    LocalDateTime getTimestampUpdated();
    boolean isIsPrivate();
    boolean isIsVerified();
    @Value("#{target.getNumberOfFollowers()}")
    int getNumberOfFollowers();
    List<InstagramUser> getFollowers();
    Set<Post> getTaggedIn();
    Set<Post> getLikedPosts();
    @Value("#{target.getNumberOfPosts()}")
    int getNumberOfPosts();
    List<Post> getPosts();
    List<Comment> getComments();
}
