FROM maven:3.6.3-jdk-11
EXPOSE 8080
ADD ./ ./
RUN mvn clean install
ENTRYPOINT ["mvn", "spring-boot:run"]