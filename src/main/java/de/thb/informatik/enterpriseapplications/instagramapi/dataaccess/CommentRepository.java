package de.thb.informatik.enterpriseapplications.instagramapi.dataaccess;

import de.thb.informatik.enterpriseapplications.instagramapi.model.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "comments", path = "comments")
public interface CommentRepository extends JpaRepository<Comment, Long> {
}
