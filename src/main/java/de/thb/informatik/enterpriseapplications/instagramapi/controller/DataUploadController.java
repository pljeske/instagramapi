package de.thb.informatik.enterpriseapplications.instagramapi.controller;

import de.thb.informatik.enterpriseapplications.instagramapi.dataaccess.InstagramUserService;
import de.thb.informatik.enterpriseapplications.instagramapi.model.dto.InstagramUserDto;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

// TODO: Do you need UPDATE method or is it handled by Spring Data REST?

@RestController
public class DataUploadController {
    private final InstagramUserService userService;
    private final ModelMapper modelMapper;
    private static final Logger LOGGER = LoggerFactory.getLogger(DataUploadController.class);

    public DataUploadController(@Autowired InstagramUserService userService,
                                @Autowired ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
        this.userService = userService;
    }

    @PostMapping("/api/scraperupload")
    public HttpStatus uploadData(@RequestBody Iterable<InstagramUserDto> data) {
        for (InstagramUserDto user : data) {
            userService.persistUserDto(user);
        }
        return HttpStatus.CREATED;
    }

//    @GetMapping("/users/{id}/followed_by")
//    public List<InstagramUserProjection> getAllUsersUserIsFollowedBy(@PathVariable("id") long id) {
//        InstagramUser user = userService.getUser(id);
//        List<InstagramUserProjection> users = userService.getUsersWhoFollowUser(user);
//        return users;
//    }
//
//    @GetMapping("/users/{id}/followers/not_following_back")
//    public List<InstagramUser> getFollowersNotFollowingBack(@PathVariable("id") long id) {
//        InstagramUser user = userService.getUser(id);
//        List<InstagramUser> followers = user.getFollowers();
//        followers = followers.stream().filter(
//                follower -> !follower.getFollowers().contains(user)).collect(Collectors.toList()
//        );
//        return followers;
//
//    }

//    @GetMapping("/users/{id}/followed_by")
//    public ModelAndView getAllUsersUserIsFollowedBy(@PathVariable("id") long id, ModelMap model) {
//        String redirectString = String.format("forward:/users/search/findByFollowersUserId?userId=%d", id);
//        return new ModelAndView(redirectString, model);
//    }
}
