package de.thb.informatik.enterpriseapplications.instagramapi.model.dto;

import de.thb.informatik.enterpriseapplications.instagramapi.model.entity.Comment;
import de.thb.informatik.enterpriseapplications.instagramapi.model.entity.InstagramUser;
import de.thb.informatik.enterpriseapplications.instagramapi.model.entity.Post;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InstagramUserDto {
    private String name;
    private String businessEmail;
    private Byte[] profilePicture;
    private String biography;
    private boolean isPrivate;
    private boolean isVerified;
    private List<InstagramUser> followers;
    private Set<Post> taggedIn;
    private Set<Post> likedPosts;
    private List<Post> posts;
    private List<Comment> comments;
}
