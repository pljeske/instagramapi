package de.thb.informatik.enterpriseapplications.instagramapi;

import de.thb.informatik.enterpriseapplications.instagramapi.dataaccess.InstagramUserRepository;
import de.thb.informatik.enterpriseapplications.instagramapi.dataaccess.InstagramUserService;
import de.thb.informatik.enterpriseapplications.instagramapi.model.entity.InstagramUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDateTime;

@SpringBootApplication
@EnableSwagger2
@Import(SpringDataRestConfiguration.class)
public class InstagramapiApplication {
    @Autowired
    private InstagramUserService userService;
    @Autowired
    private InstagramUserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(InstagramapiApplication.class, args);
    }

    @Bean
    public void createTestData(){

        InstagramUser user1 = InstagramUser.builder()
                .biography("none")
                .businessEmail("user1@posteo.de")
                .isPrivate(false)
                .isVerified(true)
                .name("user1")
                .timestampUpdated(LocalDateTime.now())
                .build();
        InstagramUser user2 = InstagramUser.builder()
                .biography("none")
                .businessEmail("user2@posteo.de")
                .isPrivate(false)
                .isVerified(true)
                .name("user2")
                .timestampUpdated(LocalDateTime.now())
                .build();
        InstagramUser user3 = InstagramUser.builder()
                .biography("none")
                .businessEmail("user3@posteo.de")
                .isPrivate(false)
                .isVerified(true)
                .name("user3")
                .timestampUpdated(LocalDateTime.now())
                .build();
        user1 = userRepository.save(user1);
        user2 = userRepository.save(user2);
        user3 = userRepository.save(user3);

        user1.addFollower(user2);
        user1.addFollower(user3);
        user3.addFollower(user1);

        userRepository.save(user1);
    }
}
