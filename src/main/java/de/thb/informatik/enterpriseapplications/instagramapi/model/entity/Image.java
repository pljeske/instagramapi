package de.thb.informatik.enterpriseapplications.instagramapi.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
public class Image extends Post {
    private String url;
    @ManyToOne(optional = true, cascade = CascadeType.ALL)
    private Carousel carousel;

    @Override
    @SuppressWarnings("all")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Image image = (Image) o;
        return url.equals(image.url) &&
                Objects.equals(carousel, image.carousel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), url, carousel);
    }
}
